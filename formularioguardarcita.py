import datetime
import tkinter as tk
from tkinter import Tk, ttk
from tkinter import messagebox as mb
from tkinter import scrolledtext as st
import citas

class FormularioCitas:

    def __init__(self):
        self.cita1=citas.Citas()
        self.ventana1=tk.Tk()
        self.ventana1.title("Agenda de citas")
        self.cuaderno1 = ttk.Notebook(self.ventana1)     
        
        

        self.carga_citas()
        self.listado_completo()
        self.borrado()
        self.otras_citas()
        self.modificar()
        self.cuaderno1.grid(column=0, row=0, padx=10, pady=10)
        self.ventana1.mainloop()

    def carga_citas(self):
    
        self.pagina1 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina1, text="Carga de citas")
        self.labelframe1=ttk.LabelFrame(self.pagina1, text="Cita")  
        self.labelframe1.grid(column=0, row=0, padx=5, pady=10)
        self.label1=ttk.Label(self.labelframe1, text="Identificación:")
        self.label1.grid(column=1, row=0, padx=4, pady=4)
        self.idencarga=tk.StringVar()
        self.idencarga=ttk.Entry(self.labelframe1, textvariable=self.idencarga)
        self.idencarga.grid(column=2, row=0, padx=4, pady=4)
        horas=[]
        for x in range(24):
          horas.append(x) 
        self.n = tk.StringVar()
        self.combobox1=ttk.Combobox(self.labelframe1, 
                                  width=10, 
                                  textvariable=self.n, 
                                  values=horas)   
        self.combobox1.grid(column=1, row=1)
        self.label2=ttk.Label(self.labelframe1, text="Hora:")
        self.label2.grid(column=0, row=1)
        
        minutos=[]
        for x in range(0,51,10):
            minutos.append(x)
        self.n2 = tk.StringVar()
        self.combobox2=ttk.Combobox(self.labelframe1, 
                                  width=10, 
                                  textvariable=self.n2, 
                                  values=minutos)   
        self.combobox2.grid(column=1, row=2)
        self.label3=ttk.Label(self.labelframe1, text="Minutos:")
        self.label3.grid(column=0, row=2)


        self.label1=ttk.Label(self.labelframe1, text="Asunto:")
        self.label1.grid(column=2, row=2, padx=4, pady=4)
        self.text_area = st.ScrolledText(self.labelframe1,width=20, height=5)
        self.text_area.grid(column=2,row=3, padx=10, pady=10)
  
        self.fechahoy = datetime.datetime.now()     
        self.label4=ttk.Label(self.labelframe1,text=self.fechahoy.strftime("%x"))       
        self.label4.grid(column=0, row=0, padx=4, pady=4)

        self.boton1=ttk.Button(self.labelframe1, text="Confirmar", command=self.agregar)
        self.boton1.grid(column=1, row=7, padx=4, pady=4)

    def agregar(self):

        datos=(self.idencarga.get(),self.fechahoy.strftime('%Y-%m-%d'),self.combobox1.get()+self.combobox2.get(),self.text_area.get("1.0",'end-1c'))
        
        self.cita1.alta(datos,)
        mb.showinfo("Información", "Los datos fueron cargados")

    def listar(self):
        datos=(str(datetime.datetime.now().strftime('%Y-%m-%d')),)
       
        respuesta=self.cita1.recuperar_todos(datos)
        self.scrolledtext2.delete("1.0", tk.END)        
        for fila in respuesta:
            
            self.scrolledtext2.insert(tk.END, "Hora cita:"+str(fila[0])+
                                              "\n Día cita:"+str(fila[1])+
                                              "\n Asunto cita:"+str(fila[2])+"\n\n")

    

    
    def listado_completo(self):
        self.pagina3 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina3, text="Listado completo")
        self.labelframe2=ttk.LabelFrame(self.pagina3, text="Cita")
        self.labelframe2.grid(column=0, row=0, padx=5, pady=10)
        self.boton1=ttk.Button(self.labelframe2, text="Listado completo", command=self.listar)
        self.boton1.grid(column=0, row=0, padx=4, pady=4)
        self.scrolledtext2=st.ScrolledText(self.labelframe2, width=30, height=10)
        self.scrolledtext2.grid(column=0,row=1, padx=10, pady=10)   

    
    def borrar(self):
        
        datos=(self.entryborra.get(),)
        
        respuesta2=self.cita1.recuperar_paciente_citas(datos)
   
        self.scrolledtext2.delete("1.0", tk.END)
        for fila in respuesta2:

                     
            self.scrolledtext3.insert(tk.END, "Hora cita:"+str(fila[0])+
                                              "\n Día cita:"+str(fila[1])+
                                              "\n Identificación:"+str(fila[2])+
                                              "\n Asunto cita:"+str(fila[3])+"\n\n")

            

        self.seleccion=tk.IntVar()

        if len(respuesta2)>0:
            self.radio1=tk.Radiobutton(self.labelframe4,text=respuesta2[0][0], variable=self.seleccion, value=1)
            self.respuesta4=respuesta2[0][0]
            self.radio1.grid(column=1, row=2)
            self.radio2=tk.Radiobutton(self.labelframe4,text=respuesta2[1][0], variable=self.seleccion, value=2)
            self.respuesta5=respuesta2[1][0]
            self.radio2.grid(column=1, row=3)
            self.radio2=tk.Radiobutton(self.labelframe4,text=respuesta2[2][0], variable=self.seleccion, value=3)
            self.respuesta6=respuesta2[2][0]
            self.radio2.grid(column=1, row=4)

       
    def operar(self):
       
        if self.seleccion.get()==1:
           horaselec=str(self.respuesta4)           
         
        if self.seleccion.get()==2:
           horaselec=self.respuesta5

        if self.seleccion.get()==2:
           horaselec=self.respuesta6
    
        datos=(self.codigoborra.get(),str(horaselec)) 

        cantidad=self.cita1.baja(datos,)

       
        if cantidad==1:
           mb.showinfo("Información", "Se canceló la cita")
        else:
           mb.showinfo("Información", "No existe una cita con esa identificación")  

    
    def borrado(self):
        self.pagina4 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina4, text="Cancelar cita")
        self.labelframe4=ttk.LabelFrame(self.pagina4, text="Cita")        
        self.labelframe4.grid(column=0, row=0, padx=5, pady=10)
        self.label1=ttk.Label(self.labelframe4, text="Identificacion:")
        self.label1.grid(column=0, row=0, padx=4, pady=4)
        self.codigoborra=tk.StringVar()
        self.entryborra=ttk.Entry(self.labelframe4, textvariable=self.codigoborra)
        self.entryborra.grid(column=1, row=0, padx=4, pady=4)
        self.boton2=ttk.Button(self.labelframe4, text="Listar citas", command=self.borrar)
        self.boton2.grid(column=0, row=0, padx=4, pady=4)
        self.boton1=ttk.Button(self.labelframe4, text="Cancelar", command=self.operar)
        self.boton1.grid(column=1, row=1, padx=4, pady=4) 
       
        self.scrolledtext3=st.ScrolledText(self.labelframe4, width=30, height=10)
        self.scrolledtext3.grid(column=0,row=1, padx=10, pady=10)
       

    

    def otras_citas(self):
    
        self.pagina1 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina1, text="Carga de otras citas")
        self.labelframe3=ttk.LabelFrame(self.pagina1, text="Cita")        
        self.labelframe3.grid(column=0, row=0, padx=5, pady=10)  
        self.fechahoy = datetime.datetime.now()     
        self.label6=ttk.Label(self.labelframe3,text="LA FECHA ACTUAL ES "+self.fechahoy.strftime("%x"))       
        self.label6.grid(column=0, row=0, padx=4, pady=4)
        self.label7=ttk.Label(self.labelframe3,text="SELECCIONE LA NUEVA FECHA ")       
        self.label7.grid(column=0, row=1, padx=4, pady=4)
        self.label7=ttk.Label(self.labelframe3,text="DIA")       
        self.label7.grid(column=0, row=2, padx=4, pady=4)
         
        

        dias=[]
        for x in range(1,32):
          dias.append(x) 
        self.d = tk.StringVar()
        self.combobox4=ttk.Combobox(self.labelframe3, 
                                  width=10, 
                                  textvariable=self.d, 
                                  values=dias)   
        self.combobox4.grid(column=1, row=2)
        self.label8=ttk.Label(self.labelframe3,text="MES") 
        self.label8.grid(column=2, row=2, padx=4, pady=4)
        meses=("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre")
        self.mes=tk.StringVar()
        self.combobox3=ttk.Combobox(self.labelframe3, 
                                    width=10, 
                                    textvariable=self.mes, 
                                    values=meses,
                                    state='readonly')
        self.combobox3.grid(column=3, row=2)                            
        
        self.label9=ttk.Label(self.labelframe3,text="AÑO") 
        self.label9.grid(column=4, row=2, padx=4, pady=4)
        anios=[]
        for x in range(2006,2022):
          anios.append(x)

        self.a = tk.StringVar()
        self.combobox5=ttk.Combobox(self.labelframe3, 
                                  width=10, 
                                  textvariable=self.a, 
                                  values=anios)   
        self.combobox5.grid(column=5, row=2) 
        
        self.labelidotras=ttk.Label(self.labelframe3, text="Ingrese identificación:")
        self.labelidotras.grid(column=0, row=7, padx=5, pady=5) 
        self.idotras=tk.StringVar()
        self.idtextotras=ttk.Entry(self.labelframe3, textvariable=self.idotras)
        self.idtextotras.grid(column=1, row=7, padx=4, pady=4)
        self.boton2=ttk.Button(self.labelframe3, text="Confirmar", command=self.mostrar_2)
        self.boton2.grid(column=1, row=9, padx=4, pady=4)
    
    
    def valida_mes(self,mes):
         
       if mes=='Enero':
          mes2=1
       elif mes=='Febrero':
          mes2=2
       elif mes=='Marzo':
           mes2=3
       elif mes=='Abril':
          mes2=4
       elif mes=='Mayo':
           mes2=5   
       elif mes=='Junio':
          mes2=6
       elif mes=='Julio':
           mes2=7  
       elif mes=='Agosto':
          mes2=8
       elif mes=='Septiembre':
           mes2=9 
       elif mes=='Octubre':
          mes2=10
       elif mes=='Noviembre':
           mes2=11
       elif mes=='Diciembre':
           mes2=12
       else:
           mes2=0

       return mes2       

   
       

    def mostrar_2(self):
        
     
    
        self.dialogo=tk.Toplevel(self.ventana1)
        mes3=self.valida_mes(self.combobox3.get())
        self.fecha=str(self.combobox5.get())+"/"+str(mes3)+"/"+str(self.combobox4.get())
        
      
        

        self.label10=ttk.Label(self.dialogo, text="Ingrese hora:")
        self.label10.grid(column=0, row=0, padx=5, pady=5)  
        horas=[]
        for x in range(24):
          horas.append(x) 
        self.n = tk.StringVar()
        self.combobox10=ttk.Combobox(self.dialogo, 
                                  width=10, 
                                  textvariable=self.n, 
                                  values=horas)   
        self.combobox10.grid(column=1, row=0)
        self.combobox10.focus()
        
        minutos=[]
        for x in range(0,51,10):
            minutos.append(x)
        self.n2 = tk.StringVar()
        self.combobox12=ttk.Combobox(self.dialogo, 
                                  width=10, 
                                  textvariable=self.n2, 
                                  values=minutos)   
        self.combobox12.grid(column=1, row=1)
        self.label3=ttk.Label(self.dialogo, text="Minutos:")
        self.label3.grid(column=0, row=1)


        self.label1=ttk.Label(self.dialogo, text="Asunto:")
        self.label1.grid(column=2, row=0, padx=4, pady=4)
        self.text_area2 = st.ScrolledText(self.dialogo,width=20, height=5)
        self.text_area2.grid(column=2,row=1, padx=10, pady=10)
  
        

  
        fecha=str(self.combobox5.get())+"/"+str(mes3)+"/"+str(self.combobox4.get())
        
   
     
        
        self.boton5=ttk.Button(self.dialogo, text="Confirma de cita", command=self.agregar_2)
        self.boton5.grid(column=1, row=4, padx=4, pady=4)
        mb.showinfo("Información", "Confirme la hora")
    
    def agregar_2(self):
       
       datos=(str(self.idtextotras.get()), str(self.fecha),self.combobox10.get()+self.combobox12.get(),self.text_area2.get("1.0",'end-1c'))
       self.cita1.alta(datos,)
       mb.showinfo("Información", "Los datos fueron cargados")

    def modificar(self):
        self.pagina6 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina6, text="Modificar cita")
        self.labelframe6=ttk.LabelFrame(self.pagina6, text="Cita")
        self.labelframe6.grid(column=0, row=0, padx=5, pady=10)
        self.label1=ttk.Label(self.labelframe6, text="Identificación:")
        self.label1.grid(column=0, row=0, padx=4, pady=4)
        self.codigomod=tk.StringVar()
        self.entryid=ttk.Entry(self.labelframe6, textvariable=self.codigomod)
        self.entryid.grid(column=1, row=0, padx=4, pady=4)

        self.label6=ttk.Label(self.labelframe6,text="LA FECHA ACTUAL ES "+self.fechahoy.strftime("%x"))       
        self.label6.grid(column=0, row=1, padx=4, pady=4)
        

        self.label2=ttk.Label(self.labelframe6, text="Hora:")        
        self.label2.grid(column=0, row=2, padx=4, pady=4)

        horas=[]
        for x in range(0,24):
          horas.append(x) 
        self.n = tk.StringVar()
        self.combobox64=ttk.Combobox(self.labelframe6, 
                                  width=10, 
                                  textvariable=self.n, 
                                  values=horas)   
        self.combobox64.grid(column=1, row=2, padx=4, pady=4)
        self.combobox64.focus()
        
     
           
        self.label3=ttk.Label(self.labelframe6, text="Minutos:")        
        self.label3.grid(column=0, row=3, padx=4, pady=4)
        minutos=[]
        for x in range(0,51,10):
            minutos.append(x)
        self.n2 = tk.StringVar()
        self.combobox65=ttk.Combobox(self.labelframe6, 
                                  width=10, 
                                  textvariable=self.n2, 
                                  values=minutos)   
        self.combobox65.grid(column=1, row=3, padx=4, pady=4)
  
     
  
    
        self.label7=ttk.Label(self.labelframe6,text="SELECCIONE LA NUEVA FECHA ")       
        self.label7.grid(column=0, row=4, padx=4, pady=4)
        self.label7=ttk.Label(self.labelframe6,text="DIA")       
        self.label7.grid(column=0, row=6, padx=4, pady=4)
        dias=[]
        for x in range(1,32):
          dias.append(x) 
        self.d = tk.StringVar()
        self.combobox61=ttk.Combobox(self.labelframe6, 
                                  width=10, 
                                  textvariable=self.d, 
                                  values=dias)   
        self.combobox61.grid(column=1, row=6)
        self.label8=ttk.Label(self.labelframe6,text="MES") 
        self.label8.grid(column=3, row=6, padx=4, pady=4)
        meses=("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre")
        self.mes=tk.StringVar()
        self.combobox62=ttk.Combobox(self.labelframe6, 
                                    width=10, 
                                    textvariable=self.mes, 
                                    values=meses,
                                    state='readonly')
        self.combobox62.grid(column=4, row=6)                            
        
        self.label9=ttk.Label(self.labelframe6,text="AÑO") 
        self.label9.grid(column=5, row=6, padx=4, pady=4)
        anios=[]
        for x in range(2006,2022):
          anios.append(x)

        self.a = tk.StringVar()
        self.combobox63=ttk.Combobox(self.labelframe6, 
                                  width=10, 
                                  textvariable=self.a, 
                                  values=anios) 
        self.combobox63.grid(column=6, row=6, padx=4, pady=4)                           
    
        

    
    

        #self.boton1=ttk.Button(self.labelframe6, text="Consultar", command=self.consultar_mod)
        self.boton1.grid(column=1, row=7, padx=4, pady=4)
        self.boton2=ttk.Button(self.labelframe6, text="Modificar", command=self.modifica)
        self.boton2.grid(column=1, row=7, padx=4, pady=4)

    def modifica(self):
        mes6=self.valida_mes(self.combobox62.get())
        self.fecha=str(self.combobox63.get())+"/"+str(mes6)+"/"+str(self.combobox61.get())
        mb.showinfo(self.fecha)
        datos=(self.fecha,self.combobox64.get()+self.combobox65.get(),self.codigomod.get(),)
        cantidad=self.cita1.modificacion(datos)
        if cantidad==1:
            mb.showinfo("Información", "Se modificó la cita")
        else:
            mb.showinfo("Información", "No existe una cita con dicha identificación")

    #def consultar_mod(self):
     #   datos=(self.codigomod.get(), )
      ## if len(respuesta)>0:
            #self.descripcionmod.set(respuesta[0][0])
            #self.preciomod.set(respuesta[0][1])
        #else:
         #   self.descripcionmod.set('')
          #  self.preciomod.set('')
           # mb.showinfo("Información", "No existe una cita con dicha identificación")
        
   

aplicacion1=FormularioCitas() 