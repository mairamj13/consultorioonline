import tkinter as tk
from tkinter import ttk
from tkinter import messagebox as mb
from tkinter import scrolledtext as st
from tkcalendar import Calendar, DateEntry

import pacientes


class FormularioArticulos:
    def __init__(self):
        self.paciente1 = pacientes.Paciente()
        self.ventana1 = tk.Tk()
        self.ventana1.title("Ingreso de pacientes")
        self.cuaderno1 = ttk.Notebook(self.ventana1)
        self.carga_pacientes()
        self.consulta_por_codigo()
        self.listado_completo()
        self.borrado()
        self.modificar()
        self.cuaderno1.grid(column=0, row=0, padx=10, pady=10)
        self.ventana1.mainloop()

    def carga_pacientes(self):
        self.pagina1 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina1, text="Ingresar paciente")

        self.labelframe1 = ttk.LabelFrame(self.pagina1, text="Paciente")
        self.labelframe1.grid(column=0, row=0, padx=5, pady=10)
        self.label2 = ttk.Label(self.labelframe1, text="Tipo de documento")
        self.label2.grid(column=0, row=0, padx=4, pady=4)
        self.documento = tk.StringVar()
        docs = (
            "Cédula de Ciudadanía", "Tarjeta de Identidad", "Cédula de Extranjería", "Registro Civil",
            "Carné Diplomático",
            "Pasaporte", "NIT", "Certificado Nacido Vivo")
        self.combobox1 = ttk.Combobox(self.labelframe1,
                                      width=17,
                                      textvariable=self.documento,
                                      values=docs,
                                      state='readonly')
        self.combobox1.grid(column=1, row=0)

        self.label2 = ttk.Label(self.labelframe1, text="Número de documento")
        self.label2.grid(column=0, row=1, padx=4, pady=4)
        self.numdoc = tk.StringVar()
        self.entrynumdoc = ttk.Entry(self.labelframe1, textvariable=self.numdoc)
        self.entrynumdoc.grid(column=1, row=1, padx=4, pady=4)

        self.label4 = ttk.Label(self.labelframe1, text="Primer Nombre:")
        self.label4.grid(column=0, row=3, padx=4, pady=4)
        self.nombprim = tk.StringVar()
        self.entrynombprim = ttk.Entry(self.labelframe1, textvariable=self.nombprim)
        self.entrynombprim.grid(column=1, row=3, padx=4, pady=4)

        self.label4 = ttk.Label(self.labelframe1, text="Segundo Nombre:")
        self.label4.grid(column=0, row=4, padx=4, pady=4)
        self.nombseg = tk.StringVar()
        self.entrynombseg = ttk.Entry(self.labelframe1, textvariable=self.nombseg)
        self.entrynombseg.grid(column=1, row=4, padx=4, pady=4)

        self.label5 = ttk.Label(self.labelframe1, text="Primer Apellido:")
        self.label5.grid(column=0, row=5, padx=4, pady=4)
        self.apeprim = tk.StringVar()
        self.entryapeprim = ttk.Entry(self.labelframe1, textvariable=self.apeprim)
        self.entryapeprim.grid(column=1, row=5, padx=4, pady=4)

        self.label6 = ttk.Label(self.labelframe1, text="Segundo Apellido:")
        self.label6.grid(column=0, row=6, padx=4, pady=4)
        self.apeseg = tk.StringVar()
        self.entryapeseg = ttk.Entry(self.labelframe1, textvariable=self.apeseg)
        self.entryapeseg.grid(column=1, row=6, padx=4, pady=4)

        self.label7 = ttk.Label(self.labelframe1, text="Correo electrónico:")
        self.label7.grid(column=0, row=7, padx=4, pady=4)
        self.corelec1 = tk.StringVar()
        self.entrycorelec = ttk.Entry(self.labelframe1, textvariable=self.corelec1)
        self.entrycorelec.grid(column=1, row=7, padx=4, pady=4)

        self.label8 = ttk.Label(self.labelframe1, text="Teléfono celular:")
        self.label8.grid(column=0, row=8, padx=4, pady=4)
        self.telcel1 = tk.StringVar()
        self.entrytelcel = ttk.Entry(self.labelframe1, textvariable=self.telcel1)
        self.entrytelcel.grid(column=1, row=8, padx=4, pady=4)

        self.boton1 = ttk.Button(self.labelframe1, text="Ingresar", command=self.agregar)
        self.boton1.grid(column=1, row=9, padx=4, pady=4)

    def my_upd(self): # triggered on Button Click
        self.label3.config(self.cal.get_date()) # read and display date

    def agregar(self):
        datos=(self.combobox1.get(),self.numdoc.get(),self.nombprim.get(),self.nombseg.get(),self.apeprim.get(),self.apeseg.get(),self.corelec1.get(),self.telcel1.get())
        self.paciente1.alta(datos)
        mb.showinfo("Información", "Los datos fueron cargados")
        self.documento.set("")
        self.numdoc.set("")
        self.nombprim.set("")
        self.nombseg.set("")
        self.apeprim.set("")
        self.apeseg.set("")
        self.corelec.set("")
        self.telcel.set("")

    def consulta_por_codigo(self):
        self.pagina2 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina2, text="Consulta por identificación")
        self.labelframe2 = ttk.LabelFrame(self.pagina2, text="Paciente")
        self.labelframe2.grid(column=0, row=0, padx=5, pady=10)
        self.label1 = ttk.Label(self.labelframe2, text="Número de identificación:")
        self.label1.grid(column=0, row=0, padx=4, pady=4)
        self.identificacion2 = tk.StringVar()
        self.entryid = ttk.Entry(self.labelframe2, textvariable=self.identificacion2)
        self.entryid.grid(column=1, row=0, padx=4, pady=4)
        self.label2 = ttk.Label(self.labelframe2, text="Primer Nombre:")
        self.label2.grid(column=0, row=1, padx=4, pady=4)
        self.primnomb2 = tk.StringVar()
        self.entryprimnomb2 = ttk.Entry(self.labelframe2, textvariable=self.primnomb2, state="readonly")
        self.entryprimnomb2.grid(column=1, row=1, padx=4, pady=4)
        self.label3 = ttk.Label(self.labelframe2, text="Segundo Nombre:")
        self.label3.grid(column=0, row=2, padx=4, pady=4)
        self.segnomb2 = tk.StringVar()
        self.entrysegnomb2 = ttk.Entry(self.labelframe2, textvariable=self.segnomb2, state="readonly")
        self.entrysegnomb2.grid(column=1, row=2, padx=4, pady=4)

        self.label4 = ttk.Label(self.labelframe2, text="Primer Apellido:")
        self.label4.grid(column=0, row=3, padx=4, pady=4)
        self.primape2 = tk.StringVar()
        self.entryprimape2 = ttk.Entry(self.labelframe2, textvariable=self.primape2, state="readonly")
        self.entryprimape2.grid(column=1, row=3, padx=4, pady=4)
        self.label5 = ttk.Label(self.labelframe2, text="Segundo Apellido:")
        self.label5.grid(column=0, row=4, padx=4, pady=4)
        self.segape2 = tk.StringVar()
        self.entrysegape2 = ttk.Entry(self.labelframe2, textvariable=self.segape2, state="readonly")
        self.entrysegape2.grid(column=1, row=4, padx=4, pady=4)
        self.label6 = ttk.Label(self.labelframe2, text="Correo electrónico:")
        self.label6.grid(column=0, row=5, padx=4, pady=4)
        self.correo2 = tk.StringVar()
        self.entrycorreo2 = ttk.Entry(self.labelframe2, textvariable=self.correo2, state="readonly")
        self.entrycorreo2.grid(column=1, row=5, padx=4, pady=4)
        self.label7 = ttk.Label(self.labelframe2, text="Teléfono celular:")
        self.label7.grid(column=0, row=6, padx=4, pady=4)
        self.telcel2 = tk.StringVar()
        self.entrycel2 = ttk.Entry(self.labelframe2, textvariable=self.telcel2, state="readonly")
        self.entrycel2.grid(column=1, row=6, padx=4, pady=4)
        



        self.boton1 = ttk.Button(self.labelframe2, text="Consultar", command=self.consultar)
        self.boton1.grid(column=1, row=9, padx=4, pady=4)

    def consultar(self):
        datos = (self.identificacion2.get(),)
        respuesta = self.paciente1.consulta(datos)
        if len(respuesta) > 0:
            self.primnomb2.set(respuesta[0][0])
            self.segnomb2.set(respuesta[0][1])
            self.primape2.set(respuesta[0][2])
            self.segape2.set(respuesta[0][3])
            self.correo2.set(respuesta[0][4])
            self.telcel2.set(respuesta[0][5])

        else:
            self.primnomb2.set('')
            self.segnomb2.set('')
            mb.showinfo("Información", "No existe un paciente con esa identificación")

    def listado_completo(self):
        self.pagina3 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina3, text="Listado completo")
        self.labelframe3 = ttk.LabelFrame(self.pagina3, text="Paciente")
        self.labelframe3.grid(column=0, row=0, padx=5, pady=10)
        self.boton1 = ttk.Button(self.labelframe3, text="Listado completo", command=self.listar)
        self.boton1.grid(column=0, row=0, padx=4, pady=4)
        self.scrolledtext1 = st.ScrolledText(self.labelframe3, width=30, height=10)
        self.scrolledtext1.grid(column=0, row=1, padx=10, pady=10)

    def listar(self):
        respuesta = self.paciente1.recuperar_todos()
        self.scrolledtext1.delete("1.0", tk.END)
        for fila in respuesta:
            self.scrolledtext1.insert(tk.END, "#Documento:" + str(fila[0]) +
                                      "\nPrimer Nombre:" + str(fila[1]) +
                                      "\nSegundo Nombre:" + str(fila[2]) +
                                      "\nPrimer Apellido:" + str(fila[3]) +
                                      "\nSegundo Apellido:" + str(fila[4]) +
                                      "\nCorreo electronico:" + str(fila[5]) +
                                      "\nTelefono:" + str(fila[6]) + "\n\n")

    def borrado(self):
        self.pagina4 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina4, text="Eliminar Paciente")
        self.labelframe4 = ttk.LabelFrame(self.pagina4, text="Paciente")
        self.labelframe4.grid(column=0, row=0, padx=5, pady=10)
        self.label1 = ttk.Label(self.labelframe4, text="Número de identificación:")
        self.label1.grid(column=0, row=0, padx=4, pady=4)
        self.codigoborra = tk.StringVar()
        self.entryborra = ttk.Entry(self.labelframe4, textvariable=self.codigoborra)
        self.entryborra.grid(column=1, row=0, padx=4, pady=4)
        self.boton1 = ttk.Button(self.labelframe4, text="Borrar", command=self.borrar)
        self.boton1.grid(column=1, row=1, padx=4, pady=4)

    def borrar(self):
        datos = (self.codigoborra.get(),)
        cantidad = self.paciente1.baja(datos)
        if cantidad == 0:
            mb.showinfo("Información", "No existe un paciente con dicho numero de identificacion")
        else:
            mb.showinfo("Información", "Se borró el artículo con dicho código")

    def modifica(self):
        datos = (self.nombprimmod.get(),self.nombsegmod.get(),self.apeprimmod.get(),self.apesegmod.get(),self.corelec.get(),self.telcel.get(),self.codigomod.get())
        cantidad = self.paciente1.modificacion(datos,)
        if cantidad == 1:
            mb.showinfo("Información", "Se actualizo la información del paciente")
        else:
            mb.showinfo("Información", "No existe un paciente con dicho documento")

    def consultar_mod(self):
        datos = (self.codigomod.get(),)
        respuesta = self.paciente1.consulta(datos)
        if len(respuesta) > 0:

            self.nombprimmod.set(respuesta[0][0])
            self.nombsegmod.set(respuesta[0][1])
            self.apeprimmod.set(respuesta[0][2])
            self.apesegmod.set(respuesta[0][3])
            self.corelec.set(respuesta[0][4])
            self.telcel.set(respuesta[0][5])

        else:
            self.nombprimmod.set('')
            self.nombsegmod.set('')
            self.apeprimmod.set('')
            self.apesegmod.set('')
            self.corelec.set('')
            self.telcel.set('')
  

            mb.showinfo("Información", "No existe un paciente con dicho documento")
        

    def modificar(self):
        self.pagina5 = ttk.Frame(self.cuaderno1)
        self.cuaderno1.add(self.pagina5, text="Actualizar Paciente")
        self.labelframe5 = ttk.LabelFrame(self.pagina5, text="Paciente")
        self.labelframe5.grid(column=0, row=0, padx=5, pady=10)

        self.label1 = ttk.Label(self.labelframe5, text="Identificación:")
        self.label1.grid(column=0, row=0, padx=4, pady=4)
        self.codigomod = tk.StringVar()
        self.entrycodigo = ttk.Entry(self.labelframe5, textvariable=self.codigomod)
        self.entrycodigo.grid(column=1, row=0, padx=4, pady=4)

        self.label3 = ttk.Label(self.labelframe5, text="Primer Nombre:")
        self.label3.grid(column=2, row=1, padx=4, pady=4)
        self.nombprimmod = tk.StringVar()
        self.entrynombprim = ttk.Entry(self.labelframe5, textvariable=self.nombprimmod)
        self.entrynombprim.grid(column=3, row=1, padx=4, pady=4)

        self.label4 = ttk.Label(self.labelframe5, text="Segundo Nombre:")
        self.label4.grid(column=2, row=2, padx=4, pady=4)
        self.nombsegmod = tk.StringVar()
        self.entrynombseg = ttk.Entry(self.labelframe5, textvariable=self.nombsegmod)
        self.entrynombseg.grid(column=3, row=2, padx=4, pady=4)

        self.label5 = ttk.Label(self.labelframe5, text="Primer Apellido:")
        self.label5.grid(column=2, row=3, padx=4, pady=4)
        self.apeprimmod = tk.StringVar()
        self.entryapeprim = ttk.Entry(self.labelframe5, textvariable=self.apeprimmod)
        self.entryapeprim.grid(column=3, row=3, padx=4, pady=4)

        self.label6 = ttk.Label(self.labelframe5, text="Segundo Apellido:")
        self.label6.grid(column=2, row=4, padx=4, pady=4)
        self.apesegmod = tk.StringVar()
        self.entryapeseg = ttk.Entry(self.labelframe5, textvariable=self.apesegmod)
        self.entryapeseg.grid(column=3, row=4, padx=4, pady=4)

        self.label7 = ttk.Label(self.labelframe5, text="Correo electrónico:")
        self.label7.grid(column=2, row=7, padx=4, pady=4)
        self.corelec = tk.StringVar()
        self.entrycorelec = ttk.Entry(self.labelframe5, textvariable=self.corelec)
        self.entrycorelec.grid(column=3, row=7, padx=4, pady=4)

        self.label8 = ttk.Label(self.labelframe5, text="Teléfono celular:")
        self.label8.grid(column=2, row=8, padx=4, pady=4)
        self.telcel = tk.StringVar()
        self.entrytelcel = ttk.Entry(self.labelframe5, textvariable=self.telcel)
        self.entrytelcel.grid(column=3, row=8, padx=4, pady=4)

        self.boton1 = ttk.Button(self.labelframe5, text="Consultar", command=self.consultar_mod)
        self.boton1.grid(column=3, row=9, padx=4, pady=4)
        self.boton2 = ttk.Button(self.labelframe5, text="Modificar", command=self.modifica)
        self.boton2.grid(column=3, row=10, padx=4, pady=4)

    

aplicacion1 = FormularioArticulos()
