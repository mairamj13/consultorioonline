import mysql.connector

class Citas:

    def abrir(self):
        conexion=mysql.connector.connect(host="localhost", 
                                              user="root", 
                                              password="",
                                              database="bd1")
        return conexion


    def alta(self, datos):
        cone=self.abrir()
        cursor=cone.cursor()

        sql="INSERT INTO citas (iddocumpaci, diacita, horacita, asuntoCita) VALUES (%s,%s, %s,%s)"
        cursor.execute(sql, datos)
        cone.commit()
        cone.close()

    def recuperar_todos(self,datos):
        cone=self.abrir()
        cursor=cone.cursor()
        sql="SELECT horacita, diacita, asuntocita FROM `citas` WHERE diacita=%s ORDER BY horacita"
        cursor.execute(sql,datos)
        cone.close()
        return cursor.fetchall()
 
    def recuperar_paciente_citas(self,datos):
        cone=self.abrir()
        cursor=cone.cursor()
       
        sql="SELECT horacita, diacita, asuntocita, iddocumpaci FROM `citas` WHERE iddocumpaci=%s ORDER BY diacita, horacita"
        cursor.execute(sql,datos)
        cone.close()
        return cursor.fetchall()

    def baja(self, datos):
        cone=self.abrir()
        cursor=cone.cursor()
        sql="delete from citas where iddocumpaci=%s and horacita=%s"
        cursor.execute(sql, datos)
        cone.commit()
        cone.close()
        return cursor.rowcount # retornamos la cantidad de filas borradas

    def modificacion(self, datos):
        cone=self.abrir()
        cursor=cone.cursor()
        sql="update citas set diacita=%s, horacita=%s where iddocumpaci=%s"
        cursor.execute(sql, datos)
        cone.commit()
        cone.close()
        return cursor.rowcount # retornamos la cantidad de filas modificadas    

    