import mysql.connector
import sqlite3


class Paciente:

    def abrir(self):
        conexion = mysql.connector.connect(host="localhost",
                                           user="root",
                                           password="",
                                           database="bd1")
        return conexion

    def alta(self, datos):
        cone=self.abrir()
        cursor=cone.cursor()
        sql="INSERT INTO usuarios (Tipodocumento, Numdocumento, Primer_Nombre, Segundo_Nombre, Primer_Apellido, Segundo_Apellido, Correo_electronico, Tel_celular) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
        cursor.execute(sql, datos)
        cone.commit()
        cone.close()

    def consulta(self, datos):
        try:
            cone = self.abrir()
            cursor = cone.cursor()
            sql = "SELECT `Primer_Nombre`, `Segundo_Nombre`, `Primer_Apellido`, `Segundo_Apellido`, `Correo_electronico`, `Tel_celular` FROM `usuarios` WHERE `Numdocumento` =%s"
            cursor.execute(sql, datos)
            return cursor.fetchall()
        finally:
            cone.close()

    def recuperar_todos(self):
        try:
            cone = self.abrir()
            cursor = cone.cursor()
            sql = "SELECT `Numdocumento`,`Primer_Nombre`, `Segundo_Nombre`, `Primer_Apellido`, `Segundo_Apellido`, `Tel_celular`, `Correo_electronico` FROM `usuarios`"
            cursor.execute(sql)
            return cursor.fetchall()
        finally:
            cone.close()


    def baja(self, datos):
        try:
            cone = self.abrir()
            cursor = cone.cursor()
            sql = "DELETE FROM `usuarios` WHERE `Numdocumento`=%s"
            cursor.execute(sql, datos)
            cone.commit()
            return cursor.rowcount  # retornamos la cantidad de filas borradas
        except:
            cone.close()

    def modificacion(self, datos):
        try:
            cone = self.abrir()
            cursor = cone.cursor()
            sql = "UPDATE `usuarios` SET `Primer_Nombre`=%s,`Segundo_Nombre`=%s,`Primer_Apellido`=%s,`Segundo_Apellido`=%s,`Correo_electronico`=%s,`Tel_celular`=%s  WHERE `Numdocumento` =%s"
            cursor.execute(sql, datos)
            cone.commit()
            return cursor.rowcount  # retornamos la cantidad de filas modificadas
        finally:
            cone.close()